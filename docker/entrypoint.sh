#!/usr/bin/env bash
# entrypoint.sh

cd /app || exit

if [ ! -f "/app/var/data.db" ]; then
  mkdir -p /app/var
  php bin/console doctrine:schema:create
  sqlite3 /app/var/data.db < admin_user.sql
fi

/root/.symfony5/bin/symfony server:start