<?php

namespace App\Repository;

use App\Entity\RatholeService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RatholeService>
 *
 * @method RatholeService|null find($id, $lockMode = null, $lockVersion = null)
 * @method RatholeService|null findOneBy(array $criteria, array $orderBy = null)
 * @method RatholeService[]    findAll()
 * @method RatholeService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatholeServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RatholeService::class);
    }

    /**
     * @return array<int>
     * @throws \Doctrine\DBAL\Exception
     */
    public function getAllPortsInUse(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT bind_addr_port FROM rathole_service rs
            ORDER BY rs.bind_addr_port ASC
            ';

        $resultSet = $conn->executeQuery($sql);

        // returns an array of arrays (i.e. a raw data set)
        $ports = $resultSet->fetchAllAssociative();

        $data = [];
        foreach ($ports as $port) {
            $data[] = $port['bind_addr_port'];
        }

        return $data;
    }

//    /**
//     * @return RatholeService[] Returns an array of RatholeService objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RatholeService
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
