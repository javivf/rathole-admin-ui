<?php

namespace App\Service;

use App\Entity\AppConfiguration;
use App\Entity\RatholeService;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Yosymfony\Toml\TomlBuilder;

class RatholeConfigManager
{

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * @var \App\Entity\AppConfiguration
     */
    protected AppConfiguration $appConfig;

    public function __construct(
      EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
        $this->appConfig = new AppConfiguration();
    }

    public function getClientConfiguration(User $user): string
    {
        $tb = new TomlBuilder();
        $tb->addComment('Your client configuration made by rathole-admin-ui')
          ->addTable('client')->addValue(
            'remote_addr',
            $this->appConfig->getRatholeServerUrl().':'.$this->appConfig->getRatholeServerPort()
          );

        $services = $user->getRatholeServices()->filter(
        /** @phpstan-ignore-next-line */
          function (RatholeService $service) {
              return $service->isActive();
          }
        );

        $slugger = new AsciiSlugger();
        /** @var RatholeService $service */
        foreach ($services as $service) {
            $tb->addTable(
              'client.services.'.$slugger->slug(
                $user->getUserIdentifier()
              ).'_'.$slugger->slug($service->getName())
            )
              ->addValue('token', $service->getToken())
              ->addValue(
                'local_addr',
                $service->getLocalAddrIP().":".$service->getLocalAddrPort()
              );
        }

        if ($services->count() === 0) {
            $tb->addComment('#');
            $tb->addComment('You must create/enable a service');
            $tb->addComment('#');
        }

        return $tb->getTomlString();
    }

    public function getServerConfiguration(): string
    {
        $tb = new TomlBuilder();
        $tb->addComment('Your server configuration made by rathole-admin-ui')
          ->addTable('server')->addValue(
            'bind_addr',
            $this->appConfig->getRatholeServerUrlBindAddr().':'.$this->appConfig->getRatholeServerPort()
          );

        $services = $this->entityManager->getRepository(RatholeService::class)
          ->findBy(['active' => true]);

        $slugger = new AsciiSlugger();
        /** @var RatholeService $service */
        foreach ($services as $service) {
            $tb->addTable(
              'server.services.'.$slugger->slug(
                $service->getUser()->getUserIdentifier()
              ).'_'.$slugger->slug($service->getName())
            )
              ->addValue('token', $service->getToken())
              ->addValue(
                'bind_addr',
                $this->appConfig->getRatholeServerUrlBindAddr().":".$service->getBindAddrPort()
              );
        }

        if (count($services) === 0) {
            $tb->addComment('#');
            $tb->addComment('You must create/enable a service');
            $tb->addComment('#');
        }

        return $tb->getTomlString();

    }

}