<?php

namespace App\Entity;

use Symfony\Component\Dotenv\Dotenv;

class AppConfiguration
{

    protected int $minPortRange;

    protected int $maxPortRange;

    protected string $ratholeServerUrl;

    protected int $ratholeServerPort;

    protected string $ratholeServerUrlBindAddr;

    protected int $ratholeServerServicesLimit;

    protected string $emailSenderAddress;



    public function __construct()
    {
        $this->minPortRange = (int) $this->getVariable('MIN_PORT_RANGE') ?: 60000;
        $this->maxPortRange = (int) $this->getVariable('MAX_PORT_RANGE') ?: 65353;
        $this->ratholeServerUrl = $this->getVariable('RATHOLE_SERVER_URL') ?: "myserver.com";
        $this->ratholeServerPort = (int) $this->getVariable('RATHOLE_SERVER_PORT') ?: 2333;
        $this->ratholeServerUrlBindAddr = $this->getVariable('RATHOLE_SERVER_BIND_ADDR') ?: "0.0.0.0";
        $this->ratholeServerServicesLimit = (int) $this->getVariable('RATHOLE_SERVER_SERVICES_LIMIT') ?: 10;
        $this->emailSenderAddress = $this->getVariable('EMAIL_SENDER_ADDRESS') ?: "admin@myserver.com";
    }

    public function getMinPortRange(): int
    {
        return $this->minPortRange;
    }

    public function getMaxPortRange(): int
    {
        return $this->maxPortRange;
    }

    public function getRatholeServerUrl(): string
    {
        return $this->ratholeServerUrl;
    }

    public function getRatholeServerPort(): int
    {
        return $this->ratholeServerPort;
    }

    public function getRatholeServerUrlBindAddr(): string
    {
        return $this->ratholeServerUrlBindAddr;
    }

    public function getRatholeServerServicesLimit(): int
    {
        return $this->ratholeServerServicesLimit;
    }

    public function getEmailSenderAddress(): string
    {
        return $this->emailSenderAddress;
    }

    protected function getVariable(string $key): string|null
    {
        $variables = [];
        foreach (['../.env', '../.env.local'] as $file) {
            if (file_exists($file)) {
                $variablesDotFile = (new Dotenv())->parse((string)file_get_contents($file));
                $variables = array_merge($variables, $variablesDotFile);
            }
        }

        if (array_key_exists($key, $variables)) {
            return $variables[$key];
        }

        return null;
    }

}
