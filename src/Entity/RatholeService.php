<?php

namespace App\Entity;

use App\Repository\RatholeServiceRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RatholeServiceRepository::class)]
#[ORM\HasLifecycleCallbacks]
class RatholeService
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $token = null;

    #[ORM\ManyToOne(inversedBy: 'ratholeServices')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\Column]
    private ?bool $active = null;

    #[ORM\Column(unique: true)]
    private ?int $bind_addr_port = 0;

    #[ORM\Column(length: 20)]
    private ?string $local_addr_ip = null;

    #[ORM\Column]
    private ?int $local_addr_port = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): static
    {
        $this->token = $token;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;

        return $this;
    }

    public function getBindAddrPort(): ?int
    {
        return $this->bind_addr_port;
    }

    public function setBindAddrPort(?int $bind_addr_port): void
    {
        $this->bind_addr_port = $bind_addr_port;
    }

    public function getLocalAddrPort(): ?int
    {
        return $this->local_addr_port;
    }

    public function setLocalAddrPort(?int $local_addr_port): void
    {
        $this->local_addr_port = $local_addr_port;
    }

    public function getLocalAddrIP(): ?string
    {
        return $this->local_addr_ip;
    }

    public function setLocalAddrIP(?string $local_addr_ip): void
    {
        $this->local_addr_ip = $local_addr_ip;
    }

}
