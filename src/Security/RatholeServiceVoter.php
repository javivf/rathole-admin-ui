<?php

namespace App\Security;

use App\Entity\RatholeService;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class RatholeServiceVoter extends Voter
{
    const OWNER = 'rathole_service_owner';

    protected function supports(string $attribute, mixed $subject): bool
    {
        if ($attribute != self::OWNER) {
            return false;
        }

        if (!$subject instanceof RatholeService) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(
      string $attribute,
      mixed $subject,
      TokenInterface $token
    ): bool {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        return match ($attribute) {
            self::OWNER => $this->canUse($subject, $user),
            default => throw new \LogicException(
              'This code should not be reached!'
            )
        };
    }

    private function canUse(RatholeService $service, User $user): bool
    {
        return $user === $service->getUser() || $user->isAdmin();
    }

}