<?php

namespace App\Command;

use App\Service\RatholeConfigManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
  name: 'app:export-server-config',
  description: 'Print rathole server configuration',
)]
class ExportServerConfigCommand extends Command
{
    /**
     * @var \App\Service\RatholeConfigManager
     */
    protected RatholeConfigManager $ratholeConfigManager;

    public function __construct(RatholeConfigManager $ratholeConfigManager)
    {
        $this->ratholeConfigManager = $ratholeConfigManager;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $output->writeln($this->ratholeConfigManager->getServerConfiguration());

        return Command::SUCCESS;
    }

}
