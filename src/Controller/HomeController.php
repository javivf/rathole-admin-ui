<?php

namespace App\Controller;

use App\Entity\AppConfiguration;
use App\Entity\User;
use App\Service\RatholeConfigManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class HomeController extends AbstractController
{

    #[Route('/', name: 'app_home')]
    public function index(RatholeConfigManager $ratholeConfigManager): Response
    {
        $appConfig = new AppConfiguration();
        /** @var User $user */
        $user = $this->getUser();
        return $this->render('home/index.html.twig', [
          'client_config' => $this->isGranted(
            'ROLE_USER'
          ) ? $ratholeConfigManager->getClientConfiguration($user) : '',
          'server_config' => $this->isGranted(
            'ROLE_ADMIN'
          ) ? $ratholeConfigManager->getServerConfiguration() : '',
        ]);
    }

    #[Route('/admin/config', name: 'app_admin_config')]
    #[IsGranted('ROLE_ADMIN')]
    public function admin_config(RatholeConfigManager $ratholeConfigManager
    ): Response {
        return new Response($ratholeConfigManager->getServerConfiguration());
    }

}
