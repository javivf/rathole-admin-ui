<?php

namespace App\Controller;

use App\Entity\AppConfiguration;
use App\Entity\RatholeService;
use App\Form\RatholeServiceType;
use App\Repository\RatholeServiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/service')]
class RatholeServiceController extends AbstractController
{
    /**
     * @var \App\Entity\AppConfiguration
     */
    private AppConfiguration $appConfig;

    public function __construct()
    {
        $this->appConfig = new AppConfiguration();
    }
    #[Route('/', name: 'app_rathole_service_index', methods: ['GET'])]
    public function index(RatholeServiceRepository $ratholeServiceRepository): Response
    {
        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
            $services = $ratholeServiceRepository->findAll();
        }
        else {
            $services = $ratholeServiceRepository->findBy(['user' => $this->getUser()]);
        }

        return $this->render('rathole_service/index.html.twig', [
            'rathole_services' => $services,
            'server_url' => $this->appConfig->getRatholeServerUrl(),
        ]);
    }

    /**
     * @throws \Exception
     */
    #[Route('/new', name: 'app_rathole_service_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();
        if ($user->getRatholeServices()->count() >= $this->appConfig->getRatholeServerServicesLimit()){
            return new Response('Sorry, you cannot create more services', Response::HTTP_SERVICE_UNAVAILABLE);
        }

        $ratholeService = new RatholeService();
        $ratholeService->setToken(hash("sha256", (string) rand()));
        $ratholeService->setActive(true);
        $form = $this->createForm(RatholeServiceType::class, $ratholeService);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ratholeService->setBindAddrPort($this->getNextAvailablePort($entityManager));
            $ratholeService->setUser($user);
            $entityManager->persist($ratholeService);
            $entityManager->flush();

            return $this->redirectToRoute('app_rathole_service_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('rathole_service/new.html.twig', [
            'rathole_service' => $ratholeService,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_rathole_service_show', methods: ['GET'])]
    #[IsGranted('rathole_service_owner', 'ratholeService')]
    public function show(RatholeService $ratholeService): Response
    {
        return $this->render('rathole_service/show.html.twig', [
            'rathole_service' => $ratholeService,
            'server_url' => $this->appConfig->getRatholeServerUrl(),
        ]);
    }

    #[Route('/{id}/edit', name: 'app_rathole_service_edit', methods: ['GET', 'POST'])]
    #[IsGranted('rathole_service_owner', 'ratholeService')]
    public function edit(Request $request, RatholeService $ratholeService, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RatholeServiceType::class, $ratholeService);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_rathole_service_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('rathole_service/edit.html.twig', [
            'rathole_service' => $ratholeService,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_rathole_service_delete', methods: ['POST'])]
    #[IsGranted('rathole_service_owner', 'ratholeService')]
    public function delete(Request $request, RatholeService $ratholeService, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid(
          'delete'.$ratholeService->getId(),
          (string)$request->request->get('_token')
        )) {
            $entityManager->remove($ratholeService);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_rathole_service_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @throws \Exception
     */
    private function getNextAvailablePort(EntityManagerInterface $entityManager): mixed
    {
        /** @var RatholeServiceRepository $serviceRepository */
        $serviceRepository = $entityManager->getRepository(RatholeService::class);
        $portsInUse = $serviceRepository->getAllPortsInUse();
        $availablePorts = array_diff($this->getAllPorts(), $portsInUse);

        if (count($availablePorts) >= 1) {
            return reset($availablePorts);
        }

        throw new \Exception('No available ports');
    }

    /**
     * @return array<int>
     */
    private function getAllPorts(): array
    {
        $data = [];
        for (
          $i = $this->appConfig->getMinPortRange();
          $i <= $this->appConfig->getMaxPortRange();
          $i++
        ) {
            $data[] = $i;
        }

        return $data;
    }
}
